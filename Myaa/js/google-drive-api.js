/**
 * Load Drive API client library.
 * Samples: https://advancedweb.hu/2015/05/26/accessing-google-drive-in-javascript/
 */
function makeDriveApiCall() {
    gapi.client.load('drive', 'v3', isUploadFolderPresent);
}

/** 
 * This returns a promise with the result. 
 * One thing we should do is to first check if the folder is present, 
 * and only create it if it is not. 
 * This is a simple listing with filters to the directory mime type 
 * and not trashed (m.a.w. gedeleted door de gebruiker), 
 * then check if there is a result.
 */
function isUploadFolderPresent() {
    return gapi.client.drive.files.list({
        q: "mimeType = 'application/vnd.google-apps.folder' and trashed = false"
    }).then(function(files) {
        var folder = files.result.files;
        getFiles();
        if (folder.some(function(item) {
                return item.name === 'myap'
            })) {}
        else {
            var request = createFolder('myap');
        }
        return 'myap';
    });
}

/**
 * Drive API Helper function createFolder
 * Create a folder with the name given in the title parameter
 *
 * @param {string} title the name of the folder to be created
 */
function createFolder(name) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    var data = {
        name: name,
        mimeType: "application/vnd.google-apps.folder"
    }
    ajax.postRequest('https://www.googleapis.com/drive/v3/files',
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        'text',
        'application/json',
        accessToken);
}

/** 
 * We weten niet van te voren wat we met de bestanden gaan doen, 
 * vandaar dat je de callback methode als parameter kan meegeven.
 * Wordt er geen parameter meegegeven, gaan we ervan uit dat je
 * gewoon een lijst van de opgehaalde bestanden wilt tonen.
 * This is a simple listing with filters to the 
 * not trashed (m.a.w. gedeleted door de gebruiker).
 * 
 * @param {callbackFunction} the callback function for the ajax call
 */
var getFiles = function(callbackFunction = showFiles) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // query 
    url += '?q=';
    url += 'not+trashed';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)';
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = 'root';
    document.getElementById('currentFolderName').value = 'root';
}

/** 
 * Maak voor elk item in de lijst een li element met daarin de naam van het bestand.
 * Als het een folder is voegen we een knop toe waarop je kan klikken om de inhoud
 * van de folder te zien te krijgen
 * 
 * @param {responseText} het antwoord van de server in tekstformaat
 */
var showFiles = function(responseText) {
    var ul = document.querySelector("#list");
    ul.innerHTML = '';
    var li = document.createElement("li");
    li.innerHTML = '<button type="button" id="rootButton" class="btn btn-danger" onclick="getFiles();">Terug naar root</button>';
    ul.appendChild(li);

    var response = JSON.parse(responseText);
    for (var i = 0; i < response.files.length; i++) {
        var item = response.files[i];
        var li = document.createElement("li");
        var html = "<img src='" + item.iconLink + "'> ";
        if (item.mimeType == 'application/vnd.google-apps.folder') {
            html += '<button type="button" class="btn btn-default" onclick="getFilesInFolder(\'' +
                item.id + '\', \'' + item.name + '\');">' + item.name + '</button>';
        }
        else {
            html += item.name;
            // only html
            if (item.name.indexOf('.html') > -1) {
                html += ' <button type="button" class="btn btn-info" onclick="downloadText(' + '\'' +
                    item.id + '\', \'' + item.name + '\');"><span class="glyphicon glyphicon-open-file"></span></button>'
            }
        }
        html += ' <button type="button" class="btn btn-danger" onclick="deleteFile(' + '\'' +
            item.id + '\', \'' + item.name + '\');"><span class="glyphicon glyphicon-remove"></span></button>';
        html += ' <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="renameFolder(' + '\'' +
            item.id + '\', \'' + item.name + '\');"><span class="glyphicon glyphicon-wrench"></span></button>';
        li.innerHTML = html;
        ul.appendChild(li);
    }
}

/**
 * Show folders in the Explorer
 */
function showFoldersInExplorer() {
    var explorer = clearExplorer();
    // if no folder info is passed get it from Google Drive API
    gapi.client.drive.files.list({
        q: "mimeType = 'application/vnd.google-apps.folder' and trashed = false"
    }).then(function(files) {
        folder = files.result.files;
        folder.map(function(item) {
            var p = document.createElement('p');
            var txt = document.createElement('text');
            var btn = document.createElement("BUTTON");
            btn.className = "btn btn-default glyphicon glyphicon-folder-open";
            var textContent = document.createTextNode(item.name);
            txt.appendChild(textContent);
            explorer.appendChild(p);
            p.appendChild(btn);
            p.appendChild(txt);
            btn.onclick = function() {
                showFiles();
            };
        });
    })
}

/** 
 * We weten niet van te voren wat we met de bestanden gaan doen, 
 * vandaar dat je de callback methode als parameter kan meegeven.
 * Wordt er geen parameters meegegeven, gaan we ervan uit dat je
 * gewoon een lijst van de opgehaalde bestanden wilt tonen.
 * This is a simple listing with filters to the parent directory 
 * and not trashed (m.a.w. gedeleted door de gebruiker).
 * 
 * @param {id} de id van de te tonen folder
 * @param {name} de naam van de te tonen folder
 * @param {callbackFunction} the callback function for the ajax call
 */
function getFilesInFolder(id, name, callbackFunction = showFiles) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // query 
    url += '?q=';
    url += 'not+trashed';
    url += '+and+';
    url += '\'' + id + '\'+in+parents';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)';
    //alert(url);
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = id;
    document.getElementById('currentFolderName').value = name;
}

/**
 * Drive API Helper function createFolderInParent
 * Create a folder with the name given in the title parameter
 * in the root folder (myap)
 *
 * @param {string} title the name of the folder to be created
 */
/**
 * Drive API Helper function createFolderInParent
 * Create a folder with the name given in the title parameter
 * in the root folder (myap)
 *
 * @param {string} title the name of the folder to be created
 * @param {parentId} the id of the parent folder
 */
function createFolderInParent(name, parentId) {
    var data = {
        'name': name,
        'mimeType': "application/vnd.google-apps.folder",
        'parents': [parentId]
    };

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.postRequest('https://www.googleapis.com/drive/v3/files',
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        'text',
        'application/json',
        accessToken);
}

/**
 * Prepare the creation of a new folder
 */
function prepareCreateFolder() {
    var folderName = document.getElementById('folderName').value;
    // haal de Id van de parent folder op
    var parentFolderId = document.getElementById('currentFolderId').value;
    var parentFolderName = document.getElementById('currentFolderName').value;

    if (folderName.length > 0) {
        var regExpresion = new RegExp("[^a-zA-Z0-9_. ]+");
        if (regExpresion.test(folderName)) {
            alert('Ongeldige foldernaam: ' + folderName)
        }
        else {
            // haal alle bestanden van de google drive op en verifiëer
            // als de folder al bestaat, daarvoor geven we de callback
            // functie doesFolderExist mee
            if (parentFolderId == 'root') {
                getFiles(function(responseText) {
                    doesFolderExist(responseText,
                        folderName, parentFolderId);
                });
            }
            else {
                // als de gebruiker een map geselecteerd heeft halen
                // we alleen de bestanden in de geselecteerde map op
                getFilesInFolder(parentFolderId,
                    parentFolderName,
                    function(responseText) {
                        doesFolderExist(responseText,
                            folderName, parentFolderId);
                    });
            }
        }
    }
    else {
        alert('Typ eerst een naam voor de folder in.');
    }
}

var doesFolderExist = function(responseText, folderName, parentFolderId) {
    var response = JSON.parse(responseText);
    // check is the folder already exists
    // names not case sensitive
    if (response.files.some(function(item) {
            return item.name.toLowerCase() === folderName.toLowerCase()
        })) {
        alert('Folder met de naam ' + folderName + ' bestaat al!')
    }
    else {
        if (folderName == 'root') {
            createFolder(folderName);
        }
        else {
            createFolderInParent(folderName, parentFolderId);
        }
    }
}

/**
 * Clear files and folders in the Explorer
 */
function clearExplorer() {
    var list = document.querySelector('#list');
    list.innerHTML = '';
    return list;
}

/**
 * Drive API Helper function deleteFile
 * Delete a folder or a file based in it's Id
 * Permanently deletes a file owned by the user without moving it to the trash. 
 * If the target is a folder, all descendants owned by the user are also deleted
 * https://developers.google.com/drive/v3/reference/files/delete
 * 
 * @param id {string} the id of the item to be deleted
 * @param name {string} the name of the item to be deleted
 */
function deleteFile(id, name) {
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    ajax.deleteRequest(url,
        function(responseText) {
            alert(name + ' is gedeleted!');
        },
        accessToken);
    window.location.reload();
}

/**
 * Drive API Helper function prepareUploadText
 * Get text out from editor
 * Validate file name and call uploadText method if file name is valid
 * 
 */
function prepareUploadText() {
    // get the text from the editor
    var text = document.getElementById('editor').value;
    if (text.length > 0) {
        // get the file name
        // Eerst gedacht met parser to doen, maar dit is te streng
        // https://developer.mozilla.org/en-US/docs/Web/API/DOMParser
        // var parser = new DOMParser();
        // var doc = parser.parseFromString(text, "application/xml");
        // if (doc.getElementsByTagName('h1').length == 0) {
        var name = text.match(/<h1>(.*?)<\/h1>/g);
        if (!name) {
            alert('Voeg de template toe voor een titel in te geven!');
        }
        else {
            // we need the first match
            var fileName = name[0];
            // remove tags, dat kan waarschijnlijk eleganter...
            // maar ik heb hier al genoeg over moeten nadenken
            fileName = fileName.replace('<h1>', '');
            fileName = fileName.replace('</h1>', '');
            fileName = fileName.replace(/[^a-zA-Z0-9_. ]+/, '');
            // haal de Id van de parent folder op
            var parentFolderId = document.getElementById('currentFolderId').value;
            uploadText(fileName, text, parentFolderId);
        }
    }
    else {
        alert('Editor is leeg, je kan geen leeg bestand uploaden...');
    }
}

/**
 * Drive API Helper function uploadText
 * https://developers.google.com/drive/v3/web/manage-uploads#multipart
 * Simple upload: uploadType=media. 
 * For quick transfer of smaller files, for example, 5 MB or less.
 * 
 * @param name {string} the file name
 * @param text {string} the html content of the file
 * @param parentFolderId {string} the id of the parent folder of file
 */
function uploadText(name, text, parentFolderId) {

    var metaData = {
        'name': name + '.html',
        'parents': [parentFolderId]
    };

    var data = '--next_section\r\n' +
        'Content-Type: application/json; charset=UTF-8\r\n\r\n' +
        JSON.stringify(metaData) +
        '\r\n\r\n--next_section\r\n' +
        'Content-Type: application/text\r\n\r\n' +
        text +
        '\r\n--next_section--';

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.postRequest('https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart',
        data,
        function(responseText) {
            alert(responseText);
        },
        'text',
        'multipart/related; boundary=next_section',
        accessToken);
}

function downloadText(id, name) {
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;
    // download
    url += '?alt=media';
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    ajax.getRequest(url,
        function(responseText) {
            document.getElementById('editor').innerHTML = responseText;
        },
        'text',
        accessToken);
}

// https://www.sitepoint.com/community/t/onclick-insert-a-text-in-a-text-area/1353
function insertText(elemID, text) {
    var elem = document.getElementById(elemID);
    elem.innerHTML += text;
}

// function renameFile(fileId, newTitle) {
//   var body = {'title': newTitle};
//   var request = gapi.client.drive.files.patch({
//     'fileId': fileId,
//     'resource': body
//   });
// }

function prepareRenameFolder() {
    var id = document.getElementById('renameFolderId').value;
    var newName = document.getElementById('folderName').value;
    renameFile(id, newName);
}

function renameFolder(id, name) {
    document.getElementById('renameFolderId').value = id;
    document.getElementById('folderName').value = name;
}


function renameFile(id, newName) {
    var data = {
        'name': newName,
        'fileId': id
    };
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    var ajax = new Ajax();
    ajax.patchRequest(url,
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        'text',
        'application/json',
        accessToken);
    window.location.reload();
}

function saveEditor() {
    var text = document.getElementById('editor').value;
    if (text != "") {
        prepareUploadText()
    }else{
        uploadText()
    }
}
