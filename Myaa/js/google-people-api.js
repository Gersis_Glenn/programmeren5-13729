function makePeopleApiCall() {
    gapi.client.load('people', 'v1', getUserProfile);
}

function getUserProfile() {
    clearUserProfile();
    var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    setUserProfile(profile.getGivenName(), profile.getImageUrl());
}

function clearUserProfile() {
    var userProfile = document.getElementById('user-profile-header');
    var h1 = document.querySelector('#user-profile-header h1');
    if (h1) {
        h1.innerHTML = 'My Authoring App';
    }
    var img = document.querySelector('#user-profile-header img');
    if (img) {
        userProfile.removeChild(img);
    }
}

function setUserProfile(givenName, imageUrl) {
    var h1 = document.querySelector('#user-profile-header h1');
    if (h1) {
        h1.innerHTML = 'My Authoring App van ' + givenName;
    }
    else {
        h1 = document.createElement('h1');
        h1.appendChild(document.createTextNode('My Authoring App van ' + givenName));
        document.getElementById('user-profile-header').appendChild(h1);
    }
    var img = document.createElement('img');
    // Adds class to new element
    img.className = 'img-circle';
    // Adds Id to new element
    img.id = 'profileImage';
    img.setAttribute("src", imageUrl);
    document.getElementById('user-profile-header').appendChild(img);
}